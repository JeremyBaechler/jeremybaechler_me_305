#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@file finalmain.py
@author Jeremy Baechler
@author James Verheyden
@date 2021.12.09
@brief File used to run main tasks for term project
@details This file creates the objects necessary to run the final project tasks.
These objects are created and then passed into the tasks used and accessed in the
user interface
"""

import pyb
import utime
import touchpanel_driver
import IMU_driver
from ulab import numpy as np
import datacollection
import ustruct
import IMU_task
import motor_driver
import closedloop
import UI_task

i2c = pyb.I2C(1, pyb.I2C.MASTER)
i2c.init(pyb.I2C.MASTER)
sensor = IMU_driver.BNO055(i2c, ustruct)
sensor.op_mode(0x0C)
pin_ym = pyb.Pin (pyb.Pin.cpu.A0, pyb.Pin.ANALOG)
pin_xm = pyb.Pin (pyb.Pin.cpu.A1, pyb.Pin.OUT_PP)
pin_yp = pyb.Pin (pyb.Pin.cpu.A6, pyb.Pin.IN)
pin_xp = pyb.Pin (pyb.Pin.cpu.A7, pyb.Pin.OUT_PP)
TP = touchpanel_driver.TouchPanel(pin_ym, pin_xm, pin_yp, pin_xp, 176,100,0)
datacollection = datacollection.DataCollection(sensor, TP, utime, np)
IMU_task = IMU_task.IMU_Task('IMU_cal_coeffs')
closedloop = closedloop.ClosedLoop(utime, 10_00)
in1 = pyb.Pin(pyb.Pin.cpu.B4, pyb.Pin.OUT_PP)
in2 = pyb.Pin(pyb.Pin.cpu.B5, pyb.Pin.OUT_PP)
in3 = pyb.Pin(pyb.Pin.cpu.B0, pyb.Pin.OUT_PP)
in4 = pyb.Pin(pyb.Pin.cpu.B1, pyb.Pin.OUT_PP)
tim3 = pyb.Timer(3, freq = 20000)
t3ch1 = tim3.channel(1, pyb.Timer.PWM, pin=in1)
t3ch2 = tim3.channel(2, pyb.Timer.PWM, pin=in2)
t3ch3 = tim3.channel(3, pyb.Timer.PWM, pin=in3)
t3ch4 = tim3.channel(4, pyb.Timer.PWM, pin=in4)
motor_drv = motor_driver.DRV8847()
motor_1 = motor_drv.motor(t3ch1, t3ch2)
motor_2 = motor_drv.motor(t3ch3, t3ch4)

UI_Task = UI_task.UI_Task(10_000, sensor, TP, datacollection, IMU_task, closedloop, motor_1, motor_2)

def main():
    while True:
        UI_Task.run()
    

if __name__ == '__main__':
    main()