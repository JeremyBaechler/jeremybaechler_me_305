# -*- coding: utf-8 -*-
"""
@file touchpanel_driver.py
@author Jeremy Baechler
@author James Verheyden
@date 12/09/21
@brief This file contains the TouchPanel class definition and can be used to calibrate the
touch panel. 
@details This class can be used to read various position measurements needed for the
statespace vector. The values measured are the x and y positions as well as the
x and y linear velocities. This class object can be calibrated using various sample points on the board.
"""

import pyb, utime
from ulab import numpy as np
import os

#
pin_ym = pyb.Pin (pyb.Pin.cpu.A0, pyb.Pin.ANALOG)
pin_xm = pyb.Pin (pyb.Pin.cpu.A1, pyb.Pin.OUT_PP)
pin_yp = pyb.Pin (pyb.Pin.cpu.A6, pyb.Pin.IN)
pin_xp = pyb.Pin (pyb.Pin.cpu.A7, pyb.Pin.OUT_PP)
#ym = pyb.ADC('PA0')
##xm = pyb.ADC(pinA1)
##yp = pyb.ADC(pinA6)
##xp = pyb.ADC(pinA7)
#



class TouchPanel:
    '''
    @brief The TouchPanel class can be used to collect position data from the
    touch panel. This class creates the object that can be operated on.
    @details The TouchPanel class object is used to get ADC data for our resistive
    touch panel. This is done by setting different pins high and low and floating
    other pins in order to read the proper values from the touch panel. Once these pins are set
    to the proper output, we can take the ADC measurements and calculate calirated
    x and y positions for the contact point.
    '''
    
    def __init__(self, ym, xm, yp, xp, width, height, center):
        '''
        @brief Constructor method for TouchPanel class. 
        @param ym
        This parameter contains the pin class object that engages the y- pin.
        @param xm
        This parameter contains the pin class object that engages the x- pin.
        @param yp
        This parameter contains the pin class object that engages the y+ pin.
        @param xp
        This parameter contains the pin class object that engages the x+ pin.
        @param width
        The width of the touch panel.
        @param height
        The height of the touch panel.
        @param center
        The center of the touch panel.
        '''
        ## This parameter contains the pin class object that engages the y- pin.
        self.pin_ym = ym
        ## This parameter contains the pin class object that engages the x- pin.
        self.pin_xm = xm
        ## This parameter contains the pin class object that engages the y+ pin.
        self.pin_yp = yp
        ## This parameter contains the pin class object that engages the x+ pin.
        self.pin_xp = xp
        ## This variable stores the width of the panel
        self.width = width
        ## This variable stores the height of the panel
        self.height  = height
        ## This variable stores the center of the panel.
        self.center = center
        ## Kxx is the x-axis linear offset value used to calibrate touch measurements
        self.Kxx = self.width/4095
        ## Kxy is the x-axis shear offset value used to calibrate touch measurements
        self.Kxy = 0
        ## Kyx is the y-axis shear offset value used to calibrate touch measurements
        self.Kyx = 0
        ## Kyy is the y-axis linear offset value used to calibrate touch measurements
        self.Kyy = self.height/4095
        ## Xc is the x axis center of the touch panel.
        self.Xc = self.width/2
        ## Yc is the y axis center of the touch panel.
        self.Yc = self.height/2
        ## This filename object stores the file which will be opened or written to during calibration.
        self.filename = 'RT_cal_coeffs.txt'
        ## Serport creates an instantiation of the pyb USB_VCP class
        self.serport = pyb.USB_VCP()
        
    
    def x_scan(self):
        '''
        @brief The x-scan method scans the touch panel for x-axis touch location.
        This is done by setting xp high and xm low while floating yp.
        @return This method returns the ADC value from the x-scan procedure.
    
        '''
        self.pin_ym.init(pyb.Pin.ANALOG)
        self.pin_xm.init(pyb.Pin.OUT_PP)
        self.pin_yp.init(pyb.Pin.IN)
        self.pin_xp.init(pyb.Pin.OUT_PP)
        self.ym_ADC = pyb.ADC('PA0')
        self.pin_xp.high()
        self.pin_xm.low()
        utime.sleep_us(5)
        val = self.ym_ADC.read()
        return val
    
    def y_scan(self):
        '''
        @brief The y-scan method scans the touch panel for y-axis touch location.
        This is done by setting yp high and ym low while floating xp.
        @return This method returns the ADC value from the y-scan procedure.
        '''
        self.pin_ym.init(pyb.Pin.OUT_PP)
        self.pin_xm.init(pyb.Pin.ANALOG)
        self.pin_yp.init(pyb.Pin.OUT_PP)
        self.pin_xp.init(pyb.Pin.IN)
        self.xm_ADC = pyb.ADC('PA1')
        self.pin_yp.high()
        self.pin_ym.low()
        utime.sleep_us(5)
        val = self.xm_ADC.read()
        return val
    
    def z_scan(self):
        '''
        @brief The z_scan method enables the proper pins to read the z value from
        our touch panel. If the touchpanel has been touched, the ADC value will be lower than 4050
        meaning we have received a touch on the touchpanel.
        @return This method returns a boolean which is True when the ADC reading is
        less than 4050 and False when the ADC reading is more than 4050.
        '''
        self.pin_ym.init(pyb.Pin.ANALOG)
        self.pin_xm.init(pyb.Pin.OUT_PP)
        self.pin_yp.init(pyb.Pin.OUT_PP)
        self.pin_xp.init(pyb.Pin.IN)
        self.ym_ADC = pyb.ADC('PA0')
        self.pin_yp.high()
        self.pin_xm.low()
        utime.sleep_us(5)
        val = self.ym_ADC.read()
        return val < 4050
    
    
    def scan(self):
        '''
        @brief The scan method runs x, y, and z scan methods from previously defined class methods.
        This data is pulled together and returned in the form of a tuple.
        @return If there is a touch on the touch panel, the scan will return the
        proper scan values from x and y. If there is no touch on the touch panel,
        the method returns an array of zeros for x, y, and z.
        '''
        x_val = self.x_scan()
        y_val = self.y_scan()
        cal_x = x_val*self.Kxx + y_val*self.Kxy + self.Xc
        cal_y = x_val*self.Kyx + y_val*self.Kyy + self.Yc
        if self.z_scan():
            ADC_data = np.array([cal_x, cal_y, 1]).reshape((3,1))
        else:
            ADC_data = np.array([cal_x, cal_y, 1]).reshape((3,1))
#            print('ADC Data: ', ADC_data)
        return ADC_data
        
    
    def scan_ADC(self):
        '''
        @brief The scan_ADC method returns uncalibrated ADC data collected from the
        touch panel. This method can be used to get ADC measurements.
        @return A tuple is returned with the x scan value at index 0, the y scan value at index 1,
        and a 1 at index 2 (for the z scan)
        '''
        return (self.x_scan(), self.y_scan(), 1)
        
    
    def calibration(self):
        '''
        @brief This method checks to see if a calibration file has already been written
        to the nucleo. If this is not the case, it will prompt the user to perform 
        their own calibration. These calibration values are then written to a new file
        and stored for future use.
        '''
        
        if self.filename in os.listdir():
            print('Calibration has previously been done, calibration values have been written to board')
            with open(self.filename, 'r') as f:
            #    Read the first line of the file
                cal_data_string = f.readline()
                cal_values = [float(cal_value) for cal_value in cal_data_string.strip().split(',')]
#                print(cal_values)
                self.Kxx = cal_values[0]
                self.Kxy = cal_values[1]
                self.Kyx = cal_values[2]
                self.Kyy = cal_values[3]
                self.Xc = cal_values[4]
                self.Yc = cal_values[5]
                return np.array([self.Kxx, self.Kxy, self.Xc, self.Kyx, self.Kyy, self.Yc]).reshape((2,3))
            
        else:
            with open(self.filename, 'w') as f:
                X_prime = np.array([(-80, 40), (-40, 40), (0, 40), (40, 40), (80, 40), 
                                    (-80, 0), (-40, 0), (0, 0), (40, 0), (80, 0), 
                                    (-80, -40), (-40, -40), (0, -40), (40, -40), (80, -40)])
                print(X_prime)
                X = np.ones([15,3])
                print(X)
                print('When enough data points have been collected, press s or S to finish calibration')
                i = 0
                idx = 0
                while True:
                    if idx > 14:
                        break
                    
                    if self.z_scan() == True:
                        utime.sleep_ms(1)
                        data = self.scan_ADC()
                        x = data[0]
                        y = data[1]
                        X[i] = [x, y, 1]
                        i += 1
                        print('Move to next data point')
                        utime.sleep(1)
                        idx += 1
                        
                    if self.serport.any():
                        val = self.serport.read(1).decode()
                        if val == 's' or val == 'S':
                            break
                
                #sample = np.matrix(sample_coords)
                print(X)
                
                X_transpose = X.transpose()
                print(X_transpose)
                beta = np.dot(np.dot(np.linalg.inv(np.dot(X_transpose, X)), X_transpose), X_prime)
                print(beta)
                self.Kxx = beta[0][0]
                self.Kxy = beta[0][1]
                self.Kyx = beta[1][0]
                self.Kyy = beta[1][1]
                self.Xc = beta[2][0]
                self.Yc = beta[2][1]
                f.write(f'{self.Kxx}, {self.Kxy}, {self.Kyx}, {self.Kyy}, {self.Xc}, {self.Yc}\r\n')
        
#while True:
#    
##    print('X Position:', panel.x_scan(), 'Y Position:', panel.y_scan(), 'Z Scan:', panel.z_scan())
##    current_time = utime.ticks_us()
##    panel.scan()
#    print(panel.y_scan())
##    print(panel.scan_ADC())
#    utime.sleep(.1)
##    new_time = utime.ticks_us()
##    print(utime.ticks_diff(new_time, current_time))
##    val = ym.read()
##    print(val)
##    utime.sleep(.1)
#    
if __name__ == '__main__':
    panel = TouchPanel(pin_ym, pin_xm, pin_yp, pin_xp, 176,100,0)
#    print('Calibration is complete!', panel.calibration())
    current = utime.ticks_us()
    panel.scan()
    new = utime.ticks_us()
    diff = utime.ticks_diff(current, new)
    print(diff)
            
            

