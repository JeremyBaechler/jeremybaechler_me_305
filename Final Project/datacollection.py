# -*- coding: utf-8 -*-
"""
@file datacollection.py
@author Jeremy Baechler
@author James Verheyden
@date 12/09/21
@brief This task is used to collect statespace data from the IMU and touch panel.
@details The data collection task we can pull all pertinent data from our IMU and
touch panel. This is done by operating on the IMU and touchpanel objects passed into
the constructor.
"""





class DataCollection():
    '''
    @brief DataCollection task class for data collection.
    @details This class can be used to pull statespace data from our IMU and 
    touch panel. Once this data has been compiled, it will be returned to be used
    for data collection or for motor control.
    '''
    import ustruct
    def __init__(self, IMU, TP, utime, np):
        '''
        @brief Constructor method for DataCollection task
        @param IMU
        IMU class object
        @param TP
        TouchPanel class object
        @param utime
        utime module which had to be imported due to python error not recognizing module
        @param np
        ulab's numpy module which had to be imported due to python error not recognizing module
        '''
        self.IMU = IMU
        self.TP = TP
        self.utime = utime
        self.np = np
        
        
    def collect(self):
        '''
        @brief Method which collects data from IMU and Touch Panel.
        @details This method collects data using the passed in IMU and touch panel
        objects. The velocity of these measurements is calculated by taking two data measurements back
        to back and dividing the difference by the time difference between the two measurements.
        This is then passed into X1 and X2 for Xdot and Ydot measurements.
        @return Tuple with both X1 and X2 statespace arrays.
        '''
        euler_tuple = self.IMU.euler_angle()
        angular_vel = self.IMU.angular_vel()
        current_time = self.utime.ticks_us()
        disp = self.TP.scan()
        print('Disp:', disp)
        new_disp = self.TP.scan()
        new_time = self.utime.ticks_us()
        dtime = self.utime.ticks_diff(current_time, new_time)
        theta_y = euler_tuple[2]
        thetad_y = angular_vel[0]
        theta_x = euler_tuple[1]
        thetad_x = angular_vel[1]
       
        x = disp[0][0]
        y = disp[0][1]
        xdot = (new_disp[0][0]-disp[0][0])/dtime
        ydot = (new_disp[0][1]-disp[0][1])/dtime
        X1 = self.np.array([x, theta_y, xdot, thetad_y])
        X2 = self.np.array([y, theta_x, ydot, thetad_x])
        return (X1, X2)


if __name__ == '__main__':
    import pyb
    from math import pi
    import utime
    from ulab import numpy as np
    import IMU_driver
    import touchpanel_driver
    import ustruct
    i2c = pyb.I2C(1, pyb.I2C.MASTER)
    i2c.init(pyb.I2C.MASTER)
    sensor = IMU_driver.BNO055(i2c, ustruct)
    sensor.op_mode(0x0C)
    pin_ym = pyb.Pin (pyb.Pin.cpu.A0, pyb.Pin.ANALOG)
    pin_xm = pyb.Pin (pyb.Pin.cpu.A1, pyb.Pin.OUT_PP)
    pin_yp = pyb.Pin (pyb.Pin.cpu.A6, pyb.Pin.IN)
    pin_xp = pyb.Pin (pyb.Pin.cpu.A7, pyb.Pin.OUT_PP)
    TP = touchpanel_driver.TouchPanel(pin_ym, pin_xm, pin_yp, pin_xp, 176,100,0)
    data = DataCollection(sensor, TP, utime, np)
    while True:
        print(data.collect())

#    import pyb
#    import utime
#    import ustruct
#    import os
#    import IMU_driver
#    from ulab import numpy as np
#    filename = 'IMU_cal_coeff.txt'
#
#    i2c = pyb.I2C(1, pyb.I2C.MASTER)
#    i2c.init(pyb.I2C.MASTER)
#    sensor = IMU_driver.BNO055(i2c)
#    sensor.op_mode(0x0C)
#    
#    while True:
#        print(sensor.angular_vel())
#        utime.sleep(.2)