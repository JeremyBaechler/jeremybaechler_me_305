"""
@file IMU_task.py
@author Jeremy Baechler
@author James Verheyden
@date 12/09/21
@brief Task file which instantiates an IMU_Task class. 
@details The IMU_task class handles all the calibration steps necessary to ensure the
IMU values are calibrated. Once this is completed, the IMU can be used to store and
transmit data over the I2C bus effectively. 
"""
import pyb
import time
import ustruct
import os
import IMU_driver

i2c = pyb.I2C(1, pyb.I2C.MASTER)
i2c.init(pyb.I2C.MASTER)
sensor = IMU_driver.BNO055(i2c, ustruct)
sensor.op_mode(0x0C)

class IMU_Task():
    '''
    @brief This class can be used to create a calibration platform for our IMU.
    @details Using the calibration status of the IMU, this task checks back to see if
    the IMU is fully calibrated and then writes the proper calibration values 
    to a new file. If the 
    '''
    def __init__(self, filename):
        '''
        @brief Constructor method for IMU_Task class
        @param filename
        This input parameter is the filename that houses the IMU calibration values.
        '''
        ## Object that holds the filename string
        self.filename = filename
        
    def calibration(self):
        '''
        @brief This method is used to calibrate the IMU. 
        @details Once the IMU has been calibrated properly, the calibration values 
        are read from the IMU and written to a file for storage. If a file with 
        calibration coefficients has already been created, this method will pull
        those values and write them to the IMU.
        '''
        if self.filename in os.listdir():
#        print('File is available')
            with open(self.filename, 'r') as f:
                cal_data_string = f.readline()
                cal_values = [int(cal_value, 16) for cal_value in cal_data_string.strip().split(',')]
                print(cal_values)
                cal_bytes = bytearray(cal_values)
                i2c.mem_write(cal_bytes, 0x28, 0x55)
        
        else:
            while True:
                if sensor.cal_status() != (3,3,3,3):
                    print('Calibration Status: ', sensor.cal_status())
                else:
                    print(sensor.cal_status())
                    with open(self.filename, 'w') as f:
                        line = ''
                        cal_values = sensor.cal_coeff()
                        print(cal_values)
                        for i in range(len(cal_values)-1):
                            line += cal_values[i] + ', '
                        line += cal_values[-1]
                        f.write(line + '\r\n')
                        break
                time.sleep(.25)
        

#IMU_Task = IMU_Task('IMU_cal_coeffs.txt')
#IMU_Task.calibration()