# -*- coding: utf-8 -*-
"""
@file closedloop.py
@date 12/09/21
@author Jeremy Baechler
@author James Verheyden
@brief This file contains the ClosedLoop class definition. This is used to perform
closed loop proportional control on various real world systems.
@details With this class object defined, we are able to run the motor duty closed loop control 
using the K vector output from matlab. This value can be adjusted using the user interface task.
This class also includes a propertional gain constant which will be used to modify the
duty cycle returned from the run method.
"""
from ulab import numpy as np

class ClosedLoop:
    '''
    @brief ClosedLoop class for closed loop controls
    @details ClosedLoop class which uses the K vector from UI_Task to calculate
    the duty cycles for each motor. This is done using numpy's array multiplication.
    Once the torque required is calculated, torque to duty cycle conversions are performed
    enabling the run() class to output the proper duty cycles for each motor.
    '''
    def __init__(self, utime, period):
        '''
        @brief Class constructor used to store proportional gain values passed in
        using set_Kp method.
        @param utime
        utime module imported to work around problems recognizing pre-imported modules.
        @param period
        period sets the frequency at which the run task is performed.
        '''
        ## Variable used to store the proportional gain constant used in the control loop
        self.Kp = 20
        ## R value corresponding to motor data sheet
        self.R = 2.21
        ## Vdc value corresponding to the input voltage of the motor
        self.Vdc = 12
        ## Kt value taken from the motor data sheet, used in the duty cycle calculation
        self.Kt = 13.9
        ## Utime object used to fix inconsistencies with module importing
        self.utime = utime
        ## Period at which the run method will be performed.
        self.period = period
        ## Next time calculation which tells the program when the next run will occur.
        self.next_time = utime.ticks_add(utime.ticks_us(), self.period)
    def run(self, K, X):
        '''
        @brief This method runs the closed loop calculations.
        @param K
        K is an array of proportional gain values which are used to calculate the duty cycle
        @param X
        X is the set of statespace variables X1 and X2 corresponding to the x and y statespace vectors.
        @return This method returns a tuple of duty cycles, one for motor 1 and one for motor 2.
        '''
        current_time = self.utime.ticks_us()
        if self.utime.ticks_diff(current_time, self.next_time) >= 0:
            T1 = np.dot(-K, X[0])
            T2 = np.dot(-K, X[1])
            duty1 = self.Kp*T1*100*self.R/(4*self.Kt*self.Vdc)
            duty2 = self.Kp*T2*100*self.R/(4*self.Kt*self.Vdc)
            self.next_time = self.utime.ticks_add(self.next_time, self.period)
            return (duty1, duty2)
        
        
    
    def get_Kp(self):
        '''
        @brief Method designed to reference the self.Kp variable
        @return Returns Kp value stored in the closed loop object.
        '''
        return self.Kp
    
    def set_Kp(self, Kp):
        '''
        @brief Method that takes an input and changes the stored Kp value.
        @param Kp
        Desired proportional gain constant.
        '''
        self.Kp = Kp
        