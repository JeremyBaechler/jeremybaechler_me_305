"""
@file IMU_driver.py
@author Jeremy Baechler
@author James Verheyden
@date 12/09/21
@brief A file dedicated to constructing the BNO055 class driver.
@details This file defines the BNO055 class which is used to interface with our
IMU. This class object has methods which can change operation modes, retrieve calibration
status and coefficient values, write calibration coefficients, and retrieve euler angle data
and angular velocity data.
"""


import utime


class BNO055:
    '''
    @brief Sets up IMU class for Bosch BNO055 IMU
    @details This class is used to pass data to and from the IMU being used in 
    our term project. This IMU will be run in NDOF mode, utilizing the built in 
    data storage and computations to output necessary euler angles.
    '''
#    import ustruct
    def __init__(self, i2c, ustruct):
        '''
        @brief Constructor method for class
        @details This class takes one input, the i2c master object. With this 
        defined, we are able to send and read data from the IMU at a given location on the i2c bus.
        @param i2c
        I2C master object used to send and read signals from the I2C bus.
        '''
        ## master i2c object which is used to send and receive data along the i2c bus.
        self.i2c = i2c
        ## ustruct package must be passed into class object to fix import issues in python.
        self.ustruct = ustruct
        
        
    
    def op_mode(self, mode):
        '''
        @brief Function that defines the operation mode of the IMU
        @param mode
        The given mode, a value from 0-12 (input should be hexadecimal)
        '''
        self.i2c.mem_write(mode, 0x28, 0x3D)
    
    def cal_status(self):
        '''
        @brief Method which returns the calibration status of the IMU
        @return Returns calibration constants to determine if the IMU is calibrated
        or not.
        '''
        cal_bytes = self.i2c.mem_read(1, 0x28, 0x35)
        cal_status = ( cal_bytes[0] & 0b11,
              (cal_bytes[0] & 0b11 << 2) >> 2,
              (cal_bytes[0] & 0b11 << 4) >> 4,
              (cal_bytes[0] & 0b11 << 6) >> 6)
        return cal_status
        
    
    def cal_coeff(self):
        '''
        @brief Method which reads the calibration coefficients for the IMU
        @return Returns the hexadecimal coefficients that are stored in the IMU.
        '''
#        self.i2c.mem_write(0x00, 0x28, 0x3D)
        coefficients = self.i2c.mem_read(22, 0x28, 0x55)
        self.i2c.mem_write(0x0C, 0x28, 0x3D)
        hex_coeff = list(hex(i) for i in coefficients)
        return hex_coeff
        
    
    def write_coeff(self, coefficients):
        '''
        @brief Method to write calibration coefficients to the IMU
        @param coefficients
        Input parameter is a list of calibration coefficients to be written to 
        the IMU.
        '''
        self.i2c.mem_write(coefficients, 0x28, 0x55)
        pass
    
    def euler_angle(self):
        '''
        @brief Method which pulls Euler angle data from the IMU
        @return This method returns a tuple of Euler angles in degrees.
        '''
        eul_bytes = self.i2c.mem_read(6, 0x28, 0x1A)
#        print(eul_bytes)
        eul_signed_ints = self.ustruct.unpack('<hhh', eul_bytes)
#        print('Unpacked: ', eul_signed_ints)
        
        # Second, scale ints to get proper units
        eul_vals = tuple(eul_int/16 for eul_int in eul_signed_ints)
#        print('Scaled: ', eul_vals)
        return eul_vals
        
    def angular_vel(self):
        '''
        @brief Method to read and unpack angular velocity data from the IMU
        @return This method returns a tuple of the angular velocities of the IMU.
        '''
        angular_vel = self.i2c.mem_read(6, 0x28, 0x14)
        angular_vel_ints = self.ustruct.unpack('<hhh', angular_vel)
        angular_tuple = tuple(ang_vel/16 for ang_vel in angular_vel_ints)
        return angular_tuple


if __name__ == '__main__':
    import pyb
    import utime
#    import ustruct
    import os
    import touchpanel_driver
    filename = 'IMU_cal_coeff.txt'

    i2c = pyb.I2C(1, pyb.I2C.MASTER)
    i2c.init(pyb.I2C.MASTER)
    sensor = BNO055(i2c)
    sensor.op_mode(0x0C)
    pin_ym = pyb.Pin (pyb.Pin.cpu.A0, pyb.Pin.ANALOG)
    pin_xm = pyb.Pin (pyb.Pin.cpu.A1, pyb.Pin.OUT_PP)
    pin_yp = pyb.Pin (pyb.Pin.cpu.A6, pyb.Pin.IN)
    pin_xp = pyb.Pin (pyb.Pin.cpu.A7, pyb.Pin.OUT_PP)
    TP = touchpanel_driver.TouchPanel(pin_ym, pin_xm, pin_yp, pin_xp, 176,100,0)
    while True:
        print(sensor.angular_vel())
        utime.sleep(.2)
    
#    if filename in os.listdir():
##        print('File is available')
#        with open(filename, 'r') as f:
#            cal_data_string = f.readline()
#            cal_values = [int(cal_value, 16) for cal_value in cal_data_string.strip().split(',')]
#            print(cal_values)
#            cal_bytes = bytearray(cal_values)
#            i2c.mem_write(cal_bytes, 0x28, 0x55)
#    
#    else:
#        while True:
#            if sensor.cal_status() != (3,3,3,3):
#                print('Calibration Status: ', sensor.cal_status())
#            else:
#                print(sensor.cal_status())
#                with open(filename, 'w') as f:
#                    line = ''
#                    cal_values = sensor.cal_coeff()
#                    print(cal_values)
#                    for i in range(len(cal_values)-1):
#                        line += cal_values[i] + ', '
#                    line += cal_values[-1]
#                    f.write(line + '\r\n')
#                    break
#            time.sleep(.25)
#        