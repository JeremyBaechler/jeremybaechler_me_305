# -*- coding: utf-8 -*-
"""
Created on Thu Oct 14 12:35:18 2021
@file task_user.py
@date 2021.10.20
@author Jeremy Baechler
@author James Verheyden
"""

import pyb, utime
import array, math




class Task_User:
    ''' @brief Sets up the Task_User class
        @details Organizes all of the class methods for the task user UI.
        
        
    '''
    
    def __init__(self, period, enc1, task1, enc2, task2, motor_drv, motor_1, motor_2, ClosedLoop, z_flag, p_flag, d_flag, g_flag, s_flag):
        '''
        @brief Initiates flags, counter, timing, cache, state
        @details Initiates flags which convey when keys are pressed to perform tasks. 
        Sets a counter to zero and sets up 'next_time' which based off the period 
        determines how often to collect data. Creates the cache used by state 4 
        to collect and print encoder data. Sets the state equal to zero at the 
        end of each state iteration (with the exception for state 4).
        '''
        self.z_flag = z_flag
        self.p_flag = p_flag
        self.d_flag = d_flag
        self.g_flag = g_flag
        self.s_flag = s_flag
        self.period = period
#        self.task1 = task1
        self.enc1 = enc1
        self.task1 = task1
        self.enc2 = enc2
        self.task2 = task2
        self.motor_drv = motor_drv
        self.motor_1 = motor_1
        self.motor_2 = motor_2
        self.ClosedLoop = ClosedLoop
        self.count = 0
        self.lower = True
#        self.current_time = utime.ticks_us()
        self.next_time = utime.ticks_add(utime.ticks_us(), self.period)
        
#        self.pos_cache = array.array('f', [0]*3000)
        self.tim_cache = array.array('f', [0]*1000)
        self.delta_cache = array.array('f', [0]*1000)
        self.i = 0
        self.STATE = -1
        self.idx = 0
        self.serport = pyb.USB_VCP()
        self.time = 0
        self.num_data = ''
        self.input = 0
        self.controls = [0,0]
        self.pwm = 0
        self.j = 0

    def run(self):
        '''
        @brief Runs between states to display and collect position data
        @details States (-1,0,1,2,3,4) are used to cycle between dirrent tasks. -1 prints the help screen. 0 returns to start. 1 zeros the position encoder. 2 prints the position of the encoder. 3 prints the change in position bewtwwen the current position and the zeroed position. 4 collects position data for 30 seconds and returns a comma seperated list..
        
        '''
        current_time = utime.ticks_us()
#        print(self.current_time, self.next_time)
#        print(utime.ticks_diff(current_time, self.next_time))
        
        if utime.ticks_diff(current_time, self.next_time) >= 0:
            
#            enc1 = encoder_updated.Encoder(pyb.Pin.cpu.B6, pyb.Pin.cpu.B7, 4)
#            task1 = task_encoder.Task_Encoder()
#            print(enc1.position)
            if self.STATE == -1:
                print('Welcome to our Encoder Hardware UI!')
                print('''Please use these inputs to get data from the Nucleo: 
                    
z can be used to zero the position of encoder 1
Z can be used to zero the position of encoder 2
p can be used to print out the position of encoder 1
P can be used to print out the position of encoder 2
d can be used to print out the delta value of encoder 1
D can be used to print out the delta value of encoder 2
m can be used to set a duty cycle for motor 1
M can be used to set a duty cycle for motor 2
c or C can be used to clear a fault condition triggered by the DRV8847
g can be used to collect encoder 1 data for 30 seconds
G can be used to collect encoder 2 data for 30 seconds
s or S ends data collection prematurely during collection
r can be used to perform a step response on motor 1
R can be used to perform a step response on motor 2''')
                self.STATE = 0
                
            if self.STATE == 0:
#                print('In State 0')
                if self.serport.any():
#                    print('received an input')
                    val = self.serport.read(1).decode()
                    if val == 'z' or val == 'Z':
                        self.STATE = 1
                        if val == 'Z':
                            self.lower = False
                    elif val == 'p' or val == 'P':
                        self.STATE = 2
                        if val == 'P':
                            self.lower = False
                    elif val == 'd' or val == 'D':
                        self.STATE = 3
                        if val == 'D':
                            self.lower = False
                    elif val == 'g' or val == 'G':
                        self.STATE = 4
                        if val == 'G':
                            self.lower = False
                        print('Data collection has begun!')
                    elif val == 'c' or val == 'C':
                        self.STATE = 6
                    elif val == 'm' or val == 'M':
                        self.STATE = 7
                        print('Set Duty Cycle:')
                        if val == 'M':
                            self.lower = False
                    elif val == 'r' or val == 'R':
                        self.STATE = 8
                        if val == 'r':
                            print('Step Response for Motor 1: Enter Kp value followed by velocity setpoint')
                        if val == 'R':
                            print('Step Response for Motor 2: Enter Kp value followed by velocity setpoint')
                            self.lower = False
                    
                if self.STATE == 4 and (val == 's' or val == 'S'):
                    self.STATE = 5
                
                    
                    print('Data collection has ended')
                    
#            if self.g_flag == False:
#                
            
        
                
                
            if self.STATE == 1:
#                print('We have pressed z')
                if self.lower == True:
                    self.enc1.update()
                    self.task1.z_flag = True
                    print('Encoder 1 has been zeroed! Encoder position: ', self.task1.run(self.enc1))
                    self.task1.z_flag = False
                elif self.lower == False:
                    self.enc2.update()
                    self.task2.z_flag = True
                    print('Encoder 2 has been zeroed! Encoder position: ', self.task2.run(self.enc2))
                    self.task2.z_flag = False
                self.STATE = 0
                self.lower = True
           
            if self.STATE == 2:
#                print('We have pressed p')
                if self.lower == True:
                    self.enc1.update()
                    self.task1.p_flag = True
                    print('Encoder 1 Position: ', self.task1.run(self.enc1))
                    self.task1.p_flag = False
                elif self.lower == False:
                    self.enc2.update()
                    self.task2.p_flag = True
                    print('Encoder 2 Position: ', self.task2.run(self.enc2))
                    self.task2.p_flag = False
                self.STATE = 0
                self.lower = True

            if self.STATE == 3:
#                print('We have pressed d')
                if self.lower == True:
                    self.enc1.update()
                    self.task1.d_flag = True
#                print(self.task1.z_flag, self.task1.p_flag, self.task1.d_flag, self.task1.g_flag, self.task1.s_flag)
                    print('Delta value from Encoder 1: ', self.task1.run(self.enc1))
                    self.task1.d_flag = False
#                print(task1.z_flag, task1.p_flag, task1.d_flag, task1.g_flag, task1.s_flag)
                elif self.lower == False:
                    self.enc2.update()
                    self.task2.d_flag = True
                    print('Delta value from Encoder 2: ', self.task2.run(self.enc2))
                    self.task2.d_flag = False
                self.STATE = 0
                self.lower = True
                    
#            if self.STATE == 4:
#                
##                current_time = utime.ticks_us()
##                print('We have pressed g')
#                if self.lower == True:
#                    self.enc1.update()
#                    self.task1.g_flag = True
#                    self.pos_cache[self.idx] = self.task1.run(self.enc1)*2*3.14159/4000
#                    self.task1.g_flag = False
#                    self.tim_cache[self.idx] = self.time
#                    self.task1.d_flag = True
#                    self.delta_cache[self.idx] = self.task1.run(self.enc1)*2*3.14159/4000/.01
#                    self.task1.d_flag = False
#                    self.time += .01
#                    self.idx += 1
#                    if self.time > 30:
#                        print('Data collection has ended')
#                        self.STATE = 5
#                        
#                        self.count = 0
##                        self.STATE = 0
#                if self.serport.any():
##                        print('serport has a value')
#                        if self.serport.read(1).decode() == 's' or self.serport.read(1).decode() == 'S':
#                            self.STATE = 5
#                        
#                if self.lower == False:
#                    self.enc2.update()
#                    self.task2.g_flag = True
#                    
##                    print(round(self.time, 2), self.task2.run(self.enc2))
#                    self.pos_cache[self.idx] = self.task2.run(self.enc2)*2*3.14159/4000
#                    self.task2.g_flag = False
#                    self.tim_cache[self.idx] = self.time
#                    self.task2.d_flag = True
#                    self.delta_cache[self.idx] = self.task2.run(self.enc2)*2*3.14159/4000/.01
#                    self.task2.d_flag = False
#                    self.time += .01
#                    self.idx += 1
#                    if self.time > 30:
#                        print('Data collection has ended')
#                        self.STATE = 5
#                        
#                        self.count = 0
#                        self.STATE = 0
                    
                
            
            if self.STATE == 5:
                if self.i < len(self.pos_cache):
                        print('Position: %4.2f, Time: %4.2f, Delta: %4.2f' % (self.pos_cache[self.i], self.tim_cache[self.i], self.delta_cache[self.i]))
                        self.i += 1
                        self.lower = True
                if self.i > len(self.pos_cache):
                    self.STATE = 0
                    self.i = 0
                    self.tim_cache = array.array('f', [0]*3000)
                    self.pos_cache = array.array('f', [0]*3000)
                    self.delta_cache = array.array('f', [0]*3000)
            
            if self.STATE == 6:
                self.motor_drv.enable()
                self.STATE = 0
                
            if self.STATE == 7:
#                print('In State 7')
                if self.lower == True:
                    if self.serport.any():
                        val = self.serport.read(1)
                        if val.isdigit():
                            self.num_data += val.decode()
                            self.serport.write(val.decode())
                        elif val.decode() == '-' and len(self.num_data) == 0:
                            self.num_data += val.decode()
                            self.serport.write(val.decode())
                        elif val.decode() == '.':
                            self.num_data += val.decode()
                            self.serport.write(val.decode())
                        elif val.decode() == '\x7f' and len(self.num_data) > 0:
                            self.num_data = self.num_data[:-1]
                            self.serport.write(val.decode())
                        elif val.decode() == '\r' or val.decode() == '\n':
                            self.serport.write('\r\n')
                            self.duty1 = float(self.num_data)
#                            print('Duty Cycle is: ', self.duty1)
                            self.motor_1.set_duty(self.duty1)
                            self.STATE = 0
                            self.num_data = ''
                            
                elif self.lower == False:
                    if self.serport.any():
                        val = self.serport.read(1)
                        if val.isdigit():
                            self.num_data += val.decode()
                            self.serport.write(val.decode())
                        elif val.decode() == '-' and len(self.num_data) == 0:
                            self.num_data += val.decode()
                            self.serport.write(val.decode())
                        elif val.decode() == '.':
                            self.num_data += val.decode()
                            self.serport.write(val.decode())
                        elif val.decode() == '\x7f' and len(self.num_data) > 0:
                            self.num_data = self.num_data[:-1]
                            self.serport.write(val.decode())
                        elif val.decode() == '\r' or val.decode() == '\n':
                            self.duty2 = float(self.num_data)
#                            print('Duty Cycle is: ', self.duty2)
                            self.motor_2.set_duty(self.duty2)
                            self.STATE = 0
                            self.num_data = ''
                            self.lower = True
#                    else:
#                        print(val)
#                        self.serport.write(val.decode())
                        
            if self.STATE == 8:
                if self.lower == True:
                    if self.serport.any():
                        val = self.serport.read(1)
                        if val.isdigit():
                            self.num_data += val.decode()
                            self.serport.write(val.decode())
                        elif val.decode() == '-' and len(self.num_data) == 0:
                            self.num_data += val.decode()
                            self.serport.write(val.decode())
                        elif val.decode() == '.':
                            self.num_data += val.decode()
                            self.serport.write(val.decode())
                        elif val.decode() == '\x7f' and len(self.num_data) > 0:
                            self.num_data = self.num_data[:-1]
                            self.serport.write(val.decode())
                        elif val.decode() == '\r' or val.decode() == '\n':
                            self.serport.write('\r\n')
                            self.controls[self.input] = float(self.num_data)
#                            print('Duty Cycle is: ', self.duty1)
                            if self.input == 0:
                                self.ClosedLoop.set_Kp(self.controls[self.input])  
                            if self.input == 1:
                                self.STATE = 9
                                self.input = 0
                            
                            self.input += 1
                            self.num_data = ''
            
            if self.STATE == 9:
                if self.serport.any():
                    val = self.serport.read(1).decode()
                    if val == 's' or val == 'S':
                        self.STATE = 10
                if self.lower == True:
                    self.enc1.update()
                    self.pwm = self.ClosedLoop.run(self.controls[1], self.enc1.delta*2*math.pi/2000/.01)
                    self.delta_cache[self.idx] = self.enc1.delta*2*math.pi/2000/.01
                    self.tim_cache[self.idx] = self.time
                    print(self.enc1.delta*2*3.14159/2000/.01, -1*self.pwm)
                    self.motor_1.set_duty(-1*self.pwm)
                    self.time += .01
                    self.idx += 1
#                    print(self.STATE)
                elif self.lower == False:
                    self.enc2.update()
                    self.pwm = self.ClosedLoop.run(self.controls[1], self.enc2.delta*2*math.pi/2000/.01)
                    self.delta_cache[self.idx] = self.enc2.delta*2*math.pi/2000/.01
                    self.tim_cache[self.idx] = self.time
                    print(self.enc1.delta*2*3.14159/2000/.01, -1*self.pwm)
                    self.motor_2.set_duty(-1*self.pwm)
                    self.time += .01
                    self.idx += 1
                if self.time > 10:
                    self.STATE = 10
                    print('Data Collection has ended')
            
            if self.STATE == 10:
#                print(len(self.delta_cache))
#                print('In State 10')
                if self.j < len(self.delta_cache):
                    print(self.delta_cache[self.j], self.tim_cache[self.j])
                    self.j += 1
                else:
                    self.STATE = 0
                    self.idx = 0
                    self.j = 0
                    self.time = 0
                    
                        
            self.next_time = utime.ticks_add(self.next_time, self.period)

