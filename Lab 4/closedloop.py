# -*- coding: utf-8 -*-
"""
Created on Sun Nov 14 15:18:31 2021

@author: melab15
"""

class ClosedLoop:
    
    def __init__(self):
        self.Kp = 0
        
    
    def run(self, omega_ref, omega_meas):
        error = omega_ref-omega_meas
        duty = error * self.Kp
        return duty
        
    
    def get_Kp(self):
        return self.Kp
    
    def set_Kp(self, Kp):
        self.Kp = Kp
        