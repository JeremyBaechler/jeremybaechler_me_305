
'''
@file main.py
@date 10/21/2021
@author Jeremy Baechler
@author Jame Verheyden
'''


import pyb
import encoder_updated
import task_encoder
import task_user
import motor_driver
import closedloop


def main():
    '''
    @brief Main function that instantiates encoder and task class objects and runs user task function. 
    @details One encoder and task object are created and are used as inputs to the user task object. 
    This allows the user task to operate on each individual encoder and task, allowing for appropriate scaling later in lab. 
    Once these objects have been created, the run method is performed on our user task which runs the UI.
    '''
    
    nSLEEP = pyb.Pin(pyb.Pin.cpu.A15, pyb.Pin.OUT_PP)
    nSLEEP.high()
    
    in1 = pyb.Pin(pyb.Pin.cpu.B4, pyb.Pin.OUT_PP)
    in2 = pyb.Pin(pyb.Pin.cpu.B5, pyb.Pin.OUT_PP)
    in3 = pyb.Pin(pyb.Pin.cpu.B0, pyb.Pin.OUT_PP)
    in4 = pyb.Pin(pyb.Pin.cpu.B1, pyb.Pin.OUT_PP)
    
    tim3 = pyb.Timer(3, freq = 20000)
    t3ch1 = tim3.channel(1, pyb.Timer.PWM, pin=in1)
    t3ch2 = tim3.channel(2, pyb.Timer.PWM, pin=in2)
    t3ch3 = tim3.channel(3, pyb.Timer.PWM, pin=in3)
    t3ch4 = tim3.channel(4, pyb.Timer.PWM, pin=in4)
    
    motor_drv = motor_driver.DRV8847(nSLEEP)
    motor_1 = motor_drv.motor(t3ch1, t3ch2)
    motor_2 = motor_drv.motor(t3ch3, t3ch4)
    
    ClosedLoop = closedloop.ClosedLoop()
    
    enc1 = encoder_updated.Encoder(pyb.Pin.cpu.B6, pyb.Pin.cpu.B7, 4)
    enc2 = encoder_updated.Encoder(pyb.Pin.cpu.C6, pyb.Pin.cpu.C7, 8)
    task1 = task_encoder.Task_Encoder(False, False, False, False, False)
    task2 = task_encoder.Task_Encoder(False, False, False, False, False)
    Task = task_user.Task_User(10_000, enc1, task1, enc2, task2, motor_drv, motor_1, motor_2, ClosedLoop, False, False, False, False, False)
    
    
    
    while True:
        Task.run()
        

if __name__ == '__main__':
    main()