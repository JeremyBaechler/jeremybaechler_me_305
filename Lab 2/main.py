
'''
@file main.py
@date 10/21/2021
@author Jeremy Baechler
@author Jame Verheyden
'''


import pyb
import encoder_updated
import task_encoder
import task_user


def main():
    '''
    @brief Main function that instantiates encoder and task class objects and runs user task function. 
    @details One encoder and task object are created and are used as inputs to the user task object. 
    This allows the user task to operate on each individual encoder and task, allowing for appropriate scaling later in lab. 
    Once these objects have been created, the run method is performed on our user task which runs the UI.
    '''
    enc1 = encoder_updated.Encoder(pyb.Pin.cpu.B6, pyb.Pin.cpu.B7, 4)
    task1 = task_encoder.Task_Encoder(False, False, False, False, False)
    Task = task_user.Task_User(10_000, enc1, task1, False, False, False, False, False)
    while True:
        Task.run()
        

if __name__ == '__main__':
    main()