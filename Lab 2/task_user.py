# -*- coding: utf-8 -*-
"""
Created on Thu Oct 14 12:35:18 2021
@file task_user.py
@date 2021.10.20
@author Jeremy Baechler
@author James Verheyden
"""

import pyb, utime
import array


class Task_User:
    ''' @brief Sets up the Task_User class
        @details Organizes all of the class methods for the task user UI.
        
        
    '''
    
    def __init__(self, period, enc1, task1, z_flag, p_flag, d_flag, g_flag, s_flag):
        '''
        @brief Initiates flags, counter, timing, cache, state
        @details Initiates flags which convey when keys are pressed to perform tasks. 
        Sets a counter to zero and sets up 'next_time' which based off the period 
        determines how often to collect data. Creates the cache used by state 4 
        to collect and print encoder data. Sets the state equal to zero at the 
        end of each state iteration (with the exception for state 4).
        '''
        self.z_flag = z_flag
        self.p_flag = p_flag
        self.d_flag = d_flag
        self.g_flag = g_flag
        self.s_flag = s_flag
        self.period = period
#        self.task1 = task1
        self.enc1 = enc1
        self.task1 = task1
        self.count = 0
#        self.current_time = utime.ticks_us()
        self.next_time = utime.ticks_add(utime.ticks_us(), self.period)
        
        self.pos_cache = array.array('d', [])
        self.STATE = -1
        self.serport = pyb.USB_VCP()

    def run(self):
        '''
        @brief Runs between states to display and collect position data
        @details States (-1,0,1,2,3,4) are used to cycle between dirrent tasks. -1 prints the help screen. 0 returns to start. 1 zeros the position encoder. 2 prints the position of the encoder. 3 prints the change in position bewtwwen the current position and the zeroed position. 4 collects position data for 30 seconds and returns a comma seperated list..
        
        '''
        current_time = utime.ticks_us()
#        print(self.current_time, self.next_time)
#        print(utime.ticks_diff(current_time, self.next_time))
        
        if utime.ticks_diff(current_time, self.next_time) >= 0:
            
#            enc1 = encoder_updated.Encoder(pyb.Pin.cpu.B6, pyb.Pin.cpu.B7, 4)
#            task1 = task_encoder.Task_Encoder()
#            print(enc1.position)
            if self.serport.any():
                val = self.serport.read(1).decode()
                if val == 'z' or val == 'Z':
                    self.STATE = 1
                elif val == 'p' or val == 'P':
                    self.STATE = 2
                elif val == 'd' or val == 'D':
                    self.STATE = 3
                elif val == 'g' or val == 'G':
                    self.STATE = 4
                    print('Data collection has begun!')
                    
#            if self.g_flag == False:
#                
            if self.STATE == -1:
                print('Welcome to our Encoder Hardware UI!')
                print('''Please use these inputs to get data from the Nucleo: 
                    
z or Z can be used to zero the position of the encoder
p or P can be used to print out the position of the encoder
d or D can be used to print out the delta value of the encoder
g or G can be used to collect encoder position data for 30 seconds
s or S ends data collection prematurely during collection''')
                self.STATE = 0
        
                
                
            if self.STATE == 1:
#                print('We have pressed z')
                self.task1.z_flag = True
                print('Encoder has been zeroed! Encoder position: ', self.task1.run(self.enc1))
                self.task1.z_flag = False
                self.STATE = 0
           
            if self.STATE == 2:
#                print('We have pressed p')
                self.p_flag = True
                self.task1.p_flag = True
                print('Encoder Position: ', self.task1.run(self.enc1))
                self.p_flag = False
                self.task1.p_flag = False
                self.STATE = 0

            if self.STATE == 3:
#                print('We have pressed d')
                self.d_flag = True
                self.task1.d_flag = True
#                print(self.task1.z_flag, self.task1.p_flag, self.task1.d_flag, self.task1.g_flag, self.task1.s_flag)
                print('Delta value from Encoder: ', self.task1.run(self.enc1))
                self.d_flag = False
                self.task1.d_flag = False
#                print(task1.z_flag, task1.p_flag, task1.d_flag, task1.g_flag, task1.s_flag)
                self.STATE = 0
                    
            if self.STATE == 4:
                
#                current_time = utime.ticks_us()
#                print('We have pressed g')
                self.g_flag = True
                self.task1.g_flag = True
#                print('current time: ', current_time)
#                print('period: ', self.period)
#                self.timecache.append(self.count)
                self.pos_cache.append(self.task1.run(self.enc1))
#                self.count += .01
#                print(self.cache)
                if self.count > 30:
                    print('Data collection has ended')
                    for i in range(len(self.pos_cache)):
                        print('Position: ', self.pos_cache[i])
                    self.count = 0
                    self.STATE = 0
                if self.serport.any():
#                    print('serport has a value')
                    if self.serport.read(1).decode() == 's' or self.serport.read(1).decode() == 'S':
                        for i in range(len(self.pos_cache)):
                            print('Position: ', self.pos_cache[i])
                            self.count = 0
                        self.STATE = 0
                        
            self.next_time = utime.ticks_add(self.next_time, self.period)

