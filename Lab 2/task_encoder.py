# -*- coding: utf-8 -*-
"""
Created on Thu Oct 14 14:33:41 2021
@file task_encoder.py
@date 2021.10.20
@author Jeremy Baechler
@author James Verheyden
"""



class Task_Encoder:
    ''' @brief Sets up the Task_Encoder class
        @details Organizes all of the class methods for the Task_Encoder FSM.
        
        
    '''
    
    def __init__(self, z_flag = False, p_flag = False, d_flag = False, g_flag = False, s_flag = False):
        ''' 
        @brief Task_Encoder class, class inputs are flag values passed in from task_user.py.
        @details Class inputs are used to determine what encoder value needs to be returned. These flags are initialized to False if no input value is defined.
        '''
        self.z_flag = z_flag
        self.p_flag = p_flag
        self.d_flag = d_flag
        self.g_flag = g_flag
        self.s_flag = s_flag
        
    def run(self, encoder, z_flag = False, p_flag = False, d_flag = False, g_flag = False, s_flag = False):
        '''
        @brief Returns outputs based on flag
        @details When a flag is activated via keypress, the associated task's value is returned. Z zeros the encoder position. P gives the current encoder position. D gives the difference is position from the zsro and current positions. G records data for 30 seconds before outing. S cancels G and gives output.
        @return Returns encoder position for z, p, and g, as well as the change in encoder position for d.
        '''
#        print('inside encoder task')
#        print(self.z_flag)
        encoder.update()
#        print(encoder.position)
        if self.z_flag == True:
            encoder.set_position(0)
            return encoder.position
        
        if self.p_flag == True:
#            print('p_flag has been raised')
            return encoder.position
        
        if self.d_flag == True:
#            print('d_flag has been raised')
            return encoder.delta
        
        if self.g_flag == True:
#            print('g_flag has been raised')
            return encoder.position
        
        
        # print(encoder.delta)
        

