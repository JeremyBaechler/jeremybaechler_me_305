# -*- coding: utf-8 -*-
"""
@file encoder_updated.py
@author Jeremy Baechler
@author James Verheyden
@date 2021.10.20
@brief This file constructs the Encoder class used to update, set, and get various
data values from an encoder.
"""
import pyb


#while(True):
#    print(tim4.counter())

class Encoder:
    ''' @brief Sets up the Encoder class
        @details Organizes all of the information and formating for encoding data.
    '''
   
    def __init__(self, pin1, pin2, timerchannel):
        ''' @brief Configures variables upon initiation
            @details Corelates variables and inputs from the board. Sets up storage, position, count, and delta at zero.
        '''
        
        self.pin01 = pyb.Pin (pin1)
        self.pin02 = pyb.Pin (pin2)
        self.tim4 = pyb.Timer(timerchannel)
        self.tim4.init(prescaler = 0, period = 65535)
        self.t4ch1 = self.tim4.channel(1, pyb.Timer.ENC_AB, pin = self.pin01)
        self.t4ch2 = self.tim4.channel(2, pyb.Timer.ENC_AB, pin = self.pin02)
        
        self.storage = [0,0]
        self.new_position = 0
        self.net_position = 0
        self.position = 0
        self.count = 0
        self.delta = 0
#        print('Creating encoder object')
    def update(self):
        ''' @brief Updates output values from board.
            @details Configures current data from board. Allows for uninterupted counting past the autoreload value.
        '''
        self.storage[0] = self.count
        self.count = self.tim4.counter()
        self.storage[1] = self.count
        self.delta = self.storage[1] - self.storage[0]
        if self.delta >= 65536/2:
            self.delta -= 65535
        if self.delta <= -65536/2:
            self.delta += 65535
        self.position += self.delta
#        print('Reading encoder count and updating posistion and delta values')
    def get_position(self):
        ''' @brief Gives current position
            @return number indicating angular position
        '''
        return self.position
        
    def set_position(self, position):
        ''' @brief Set position
            @details Sets indexed position to input
        '''
        self.position = abs(position) % 65535
#        print('Setting position and delta values')
    def get_delta(self):
        ''' @brief Change in position (delta)
            @details Gives the change in position between current position and previously defined datum
        '''
#        print('inside get delta function')
        return self.delta
    
    
    
    
    
    
#encoder1 = Encoder(pyb.Pin.cpu.B6, pyb.Pin.cpu.B7, 4)    
#
#while True:
#    encoder1.update()    
#    print(encoder1.position)   
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
#if __name__ == '__main__':
#    main()
    