# -*- coding: utf-8 -*-
"""
@file motor_driver.py
@author Jeremy Baechler
@author James Verheyden
@date 11/8/21
@brief A file to handle the construction of the DRV8847 and Motor classes. These
classes are then referenced in task_user.py for various uses.
"""

import pyb, utime



#nSLEEP.high()
#t3ch1.pulse_width_percent(100)
#t3ch2.pulse_width_percent(0)

class DRV8847:
    ''' 
    @brief A motor driver class for the DRV8847 object
    @details Objects of this class can be used to configure the DRV8847
    motor driver and to create one or more objects of the 
    Motor class which can be used to perform motor control.
    '''
    
    def __init__(self, nSLEEP):
        ''' 
        @brief Constructor method for DRV8847 driver class
        @details Initializes the necessary internal variables used to perform various methods.
        @param nSLEEP
        This parameter can be used to turn the motor driver on or off. A high nSLEEP value 
        allows the motor to turn on while a low nSLEEP value prohibity the motors from spinning.
        '''
        ## Class object used to reference the proper driver pin on the nucleo.
        self.pinB2 = pyb.Pin (pyb.Pin.cpu.B2)
        ## Fault object triggered by the nucleo when motor faults occur. This fault is called back to the self.fault_cb method.
        self.Fault = pyb.ExtInt(self.pinB2, mode = pyb.ExtInt.IRQ_FALLING, pull = pyb.Pin.PULL_NONE, callback = self.fault_cb)
        ## nSLEEP object is used to set the sleep pin high or low depending on what is required of the motor driver.
        self.nSLEEP = nSLEEP
        
    
    def enable(self):
        '''
        @brief Enables the motor driver
        @details This method enables the motors to spin after a fault condition occurs. The fault condition
        is disabled, the nSLEEP pin is set high, and after 25 microseconds the fault condition is enabled again.
        '''
        self.Fault.disable()
        self.nSLEEP.high()
        utime.sleep_us(25)
        self.Fault.enable()
        
    
    def disable(self):
        '''
        @brief Method that disables the motors
        @details This method uses the nSLEEP pin to turn off the motors. If this method
        is called, the motors will stop spinning.
        '''
        self.nSLEEP.low()
        
    
    def fault_cb(self, IRQ_src):
        '''
        @brief Method to run if fault condition is triggered
        @details If fault condition is triggered through self.Fault, this method is
        called. When the fault occurs we want to turn the motors off so we set the nSLEEP
        pin low.
        @param IRQ_src
        Fault object passed into function which is defined by the nucleo board.
        '''
#        print('Fault has been triggered')
        self.nSLEEP.low()
        
    
    def motor(self, timchan1, timchan2):
        '''
        @brief initializes motor using given timer channels.
        @details Motor class is referenced to create Motor objects using this method. 
        @param timchan1
        First timer channel corresponding to the motor being driven.
        @param timchan2
        Second timer channel corresponding to the motor being driven.
        '''
        return Motor(timchan1, timchan2)

class Motor:
    '''
    @brief      Initializes and return a motor object associated with the DRV8847.
    @details    Objects of this class should not be instantiated 
                directly. Instead create a DRV8847 object and use
                that to create Motor objects using the method
                DRV8847.motor().
    '''
    def __init__(self, timchan1, timchan2):
        '''
        @brief Constructor class used to define an instance of a motor class object.
        @details This constructor takes two inputs for each timer channel and create
        new motor objects with them. 
        @param timchan1
        First channel used to drive the motor.
        @param timchan2
        Second timer channel which sends signals to drive motor.
        '''
        self.timchan1 = timchan1
        self.timchan2 = timchan2
        
    
    def set_duty(self, duty):
        '''
        @brief Method to alter the PWM duty cycle of the signal for each motor.
        @details Using the pulse_width_percent() method in the timer class the 
        motor PWM signal can be adjusted. The input duty cycle is filtered by
        logic statements in order to drive the motor in the proper direction.
        @param duty
        The desired duty cycle set point for the motor.
        '''
        if duty >= 0 and duty <= 100:
            self.timchan1.pulse_width_percent(duty)
            self.timchan2.pulse_width_percent(0)
        elif duty < 0 and duty >= -100:
            self.timchan1.pulse_width_percent(0)
            self.timchan2.pulse_width_percent(abs(duty))
        else:
            print('Motor duty cycle must be between -100 and 100')
    
#if __name__ == '__main__':
    
    
    
    

    
    
    
    
#    print('Running Motor Duty')
#    
#    motor_1.set_duty(100)
#    motor_2.set_duty(60)
        
        
        
        
        